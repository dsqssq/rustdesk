### RustDesk | 开源远程桌面软件

这里仅供二进制文件下载，源码请移步[GITHUB](https://github.com/rustdesk/rustdesk)

本项目已经全部开源，如果您觉得帮到了你，请给 https://github.com/rustdesk/rustdesk 一个Star

有任何疑问可以去 https://github.com/rustdesk/rustdesk/discussions 讨论，本论坛为英语论坛，如果英语不好，可以用百度翻译后张贴。

如果发现Bug，**请到[GITHUB](https://github.com/rustdesk/rustdesk)提交Issue**，如果英语不好，可以用百度翻译后张贴。

### 友情提示：提问前Star是很礼貌和友善的行为，被问者也会更开心回答您的问题。

[**下载**](https://gitee.com/rustdesk/rustdesk/releases)

[官网](https://rustdesk.com)

[GITHUB](https://github.com/rustdesk/rustdesk)

[国内镜像](https://gitee.com/mirrors/rustdesk)
